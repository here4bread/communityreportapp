//
//  CallOutView.h
//  GovHack
//
//  Created by Aiswarya kodali on 29/7/17.
//  Copyright © 2017 GovHack. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallOutView : UIView
    @property (strong, nonatomic) IBOutlet UIButton *reportButton;
@property (strong, nonatomic) IBOutlet UILabel *facilitytype;
@property (strong, nonatomic) IBOutlet UILabel *facilityName;

@end
