//
//  CustomMarker.h
//  GovHack
//
//  Created by Aiswarya kodali on 30/7/17.
//  Copyright © 2017 GovHack. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "Facility.h"
@interface CustomMarker : GMSMarker

@property (nonatomic) int markerID;
@property (nonatomic) int order;
@property (strong, nonatomic) Facility* referenceObject;
@property (nonatomic) BOOL selected;

@end
