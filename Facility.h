//
//  Facility.h
//  GovHack
//
//  Created by Aiswarya kodali on 29/7/17.
//  Copyright © 2017 GovHack. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Facility : NSObject
@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString * operatorName;
@property (nonatomic,strong) NSString * type;
@property (nonatomic,strong) NSString * reference;
@property (nonatomic) double lat;
@property (nonatomic) double longt;

@end
