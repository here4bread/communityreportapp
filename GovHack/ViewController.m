//
//  ViewController.m
//  GovHack
//
//  Created by Aiswarya kodali on 29/7/17.
//  Copyright © 2017 GovHack. All rights reserved.
//

#import "ViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "CallOutView.h"
#import "GovHackServices.h"
#import "Facility.h"
#import <CoreLocation/CoreLocation.h>
#import "ReportFormTableViewController.h"
#import "CustomMarker.h"

@interface ViewController ()<GMSMapViewDelegate, CLLocationManagerDelegate>
@property (strong,nonatomic) UIView *calloutView;
@property(strong,nonatomic) NSMutableArray *communityDataArray;
@property(strong,nonatomic) CLLocationManager *locationManger;
@property (strong, nonatomic) IBOutlet UIView *thankyouView;
@property (strong,nonatomic)UIView *thankYouMessageView;
@property (nonatomic,strong)NSString * nameFOrReport;
@property (strong, nonatomic) Facility *selectedfac;
@property(strong,nonatomic)GMSMapView *mapview;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.thankYouMessageView  = [[UIView alloc] initWithFrame:self.thankyouView.bounds];
    self.thankYouMessageView.backgroundColor=[UIColor whiteColor];
    
    
    
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL isSubmitted = [userDefaults boolForKey:@"sr"];
    
    if (isSubmitted) {
        //elf.thankYouMessageView.hidden = NO;
        [self.view bringSubviewToFront:_thankyouView];
        [self performSelector:@selector(hideTheThankyouView) withObject:self afterDelay:3.0 ];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setBool:NO forKey:@"sr"];
        [userDefaults synchronize];
    }else{
        [self.thankyouView setHidden:YES];
    }
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1.0/255.0 green:181.0/255.0 blue:0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    
    
    
    self.locationManger = [[CLLocationManager alloc] init];
    self.locationManger.delegate=self;
    self.locationManger.desiredAccuracy=kCLLocationAccuracyBest;
    self.locationManger.distanceFilter=kCLDistanceFilterNone;
    
    if ([self.locationManger respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManger requestAlwaysAuthorization];
    }
    [self.locationManger startMonitoringSignificantLocationChanges];
    [self.locationManger startUpdatingLocation];
    
    
    
    
}
-(void)hideTheThankyouView {
    
    [self.thankYouMessageView setHidden:YES];
    [self.thankyouView setHidden:YES];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"here 4 bread");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL isSubmitted = [userDefaults boolForKey:@"sr"];
    
    
    if (isSubmitted) {
        self.thankyouView.hidden = NO;
        [self performSelector:@selector(hideTheThankyouView) withObject:self afterDelay:5.0 ];
        [self.view addSubview:self.thankyouView];
        [self.view bringSubviewToFront:_thankyouView];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setBool:NO forKey:@"sr"];
        [userDefaults synchronize];
    }
    
    
}






- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    [self.locationManger stopUpdatingLocation];
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                            longitude:newLocation.coordinate.longitude
                                                                 zoom:20];
    self.mapview = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    self.mapview.myLocationEnabled = YES;
    self.mapview.delegate = self;
    GovHackServices *service = [[GovHackServices alloc]init];
    
    [service fetchCommunityData:newLocation success:^(NSArray *communityData) {
        
        self.communityDataArray = [[NSMutableArray alloc] init];
        
        
        for (int i = 0; i < communityData.count; i++) {
            
            NSDictionary *communityDict = communityData[i];
            Facility *newFacity  = [[Facility alloc] init];
            newFacity.name = [communityDict objectForKey:@"name"];
            newFacity.type = [communityDict objectForKey:@"type"];
            newFacity.operatorName = [communityDict objectForKey:@"operator"];
            newFacity.lat = [[communityDict objectForKey:@"latitude"] doubleValue];
            newFacity.longt = [[communityDict objectForKey:@"longitude"] doubleValue];
            
            [self.communityDataArray addObject:newFacity];
            
            
            
        }
        
        UIImage * markerIconImage;
        for (Facility *facility in self.communityDataArray) {
            if ([facility.type isEqualToString:@"public-toilets"]) {
                markerIconImage = [UIImage imageNamed:@"public toilet.png"];
            } else if([facility.type isEqualToString:@"Litter Bin"]) {
                markerIconImage = [UIImage imageNamed:@"litter bin.png"];
            }else if([facility.type isEqualToString:@"Seat"]) {
                markerIconImage = [UIImage imageNamed:@"seating.png"];
            } else if([facility.type isEqualToString:@"Bicycle Rails"]) {
                markerIconImage = [UIImage imageNamed:@"bike park.png"];
            }
            
            
            
            CustomMarker *marker = [[CustomMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(facility.lat, facility.longt);
            marker.referenceObject = facility;
            marker.markerID = [self.communityDataArray indexOfObject:facility];
            marker.icon = markerIconImage;
            
            marker.map = self.mapview;
        }
        self.view = self.mapview;
        
        
        CGRect labelFrame = CGRectMake(120,300, 530, 100);
        UILabel *myLabel = [[UILabel alloc] initWithFrame:labelFrame];
        //If you need to change the color
        [myLabel setTextColor:[UIColor whiteColor]];
        //If you need to change the system font
        [myLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:23]];
        //If you need alignment
        [myLabel setTextAlignment:NSTextAlignmentCenter];
        // The label will use an unlimited number of lines
        [myLabel setNumberOfLines:0];
       /*
        [self.view addSubview: self.thankYouMessageView];
        
        
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        BOOL isSubmitted = [userDefaults boolForKey:@"sr"];
        
        if (isSubmitted) {
            self.thankYouMessageView.hidden = NO;
        
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setBool:NO forKey:@"sr"];
            [userDefaults synchronize];
        }else {
            self.thankYouMessageView.hidden = YES;
        }
        
        
        */
        
        
    } failure:^(NSString *message) {
        
    }];
    
    
}


- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    // your code
   [self performSegueWithIdentifier:@"reportSegue" sender:self];
    
    self.mapview.selectedMarker = nil;
}


    
    - (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
    {
        
         CallOutView *view =  [[[NSBundle mainBundle]loadNibNamed:@"CallOutView" owner:self options:nil]objectAtIndex:0];
        if ([marker isKindOfClass:[CustomMarker class]]) {
            CustomMarker* parsedMarker = (CustomMarker*)marker;
            Facility *fac = parsedMarker.referenceObject;
            
            
            
            
            
            view.facilitytype.text = fac.type;
            view.facilityName.text = fac.name;
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setValue:fac.name forKey:@"n"];
            
            [userDefault synchronize];
            self.selectedfac= fac;
            self.nameFOrReport = fac.name;
            [view.reportButton addTarget:self action:@selector(annotationPressed) forControlEvents:UIControlEventTouchUpInside];
            view.userInteractionEnabled = YES;
            
            
        }
        
        
        
        
        return view;
    }


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"reportSegue"])
    {
        UINavigationController *nav = [segue destinationViewController];
        ReportFormTableViewController *root = (ReportFormTableViewController*)[nav.viewControllers objectAtIndex:0] ;
        root.fac =self.selectedfac;
        
    }
}


-(void)annotationPressed {
    
    [self performSegueWithIdentifier:@"reportSegue" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
