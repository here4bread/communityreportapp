//
//  GovHackConstants.h
//  GovHack
//
//  Created by Aiswarya kodali on 29/7/17.
//  Copyright © 2017 GovHack. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GovHackConstants.h"

@interface GovHackConstants : NSObject

typedef void (^FetchCommunityDataSuccessBlock)(NSArray* communityData);
typedef void (^FetchCommunityDataFailureBlock)(NSString* message);
typedef void (^PostReportDataSuccessBlock)(NSString* successMessage);
typedef void (^FetchReportDataFailureBlock)(NSString* message);
@end
