//
//  GovHackServices.h
//  GovHack
//
//  Created by Aiswarya kodali on 29/7/17.
//  Copyright © 2017 GovHack. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "GovHackConstants.h"
#import <CoreLocation/CoreLocation.h>

@interface GovHackServices : NSObject
- (void) fetchCommunityData:(CLLocation *)location success:(FetchCommunityDataSuccessBlock)success failure:(FetchCommunityDataFailureBlock)failure;
- (void) postReport:(NSDictionary *)reportData success:(PostReportDataSuccessBlock)success failure:(PostReportDataSuccessBlock)failure;
@end
