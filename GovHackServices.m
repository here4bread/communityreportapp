//
//  GovHackServices.m
//  GovHack
//
//  Created by Aiswarya kodali on 29/7/17.
//  Copyright © 2017 GovHack. All rights reserved.
//

#import "GovHackServices.h"
@interface GovHackServices ()

@property (nonatomic,strong)NSMutableArray *communityDataList;
@end
@implementation GovHackServices


- (void) fetchCommunityData:(CLLocation *)location success:(FetchCommunityDataSuccessBlock)success failure:(FetchCommunityDataFailureBlock)failure{
    
    
    NSString *urlstring = [NSString stringWithFormat:@"http://ec2-54-252-236-45.ap-southeast-2.compute.amazonaws.com:3000/api/assets?lat=%f&lon=%f&rad=300",location.coordinate.latitude, location.coordinate.longitude];
     AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urlstring parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@", responseObject);
        
        NSArray *communityData = [responseObject objectForKey:@"councilAssets"];
        success(communityData);
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    
    
}
- (void) postReport:(NSDictionary *)reportData success:(PostReportDataSuccessBlock)success failure:(PostReportDataSuccessBlock)failure {
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:@"http://ec2-54-252-236-45.ap-southeast-2.compute.amazonaws.com:3000/api/report" parameters:reportData progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(@"success");
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    
    
}

@end
