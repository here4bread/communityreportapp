//
//  ReportFormTableViewController.h
//  GovHack
//
//  Created by Aiswarya kodali on 29/7/17.
//  Copyright © 2017 GovHack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"Facility.h"
@interface ReportFormTableViewController : UITableViewController

@property (nonatomic,strong)NSString *Name;
@property (strong, nonatomic) Facility *fac;
@end
