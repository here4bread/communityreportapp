//
//  ReportFormTableViewController.m
//  GovHack
//
//  Created by Aiswarya kodali on 29/7/17.
//  Copyright © 2017 GovHack. All rights reserved.
//

#import "ReportFormTableViewController.h"
#import "Facility.h"
#import "GovHackServices.h"
@interface ReportFormTableViewController ()<UIPickerViewDelegate,UIPickerViewDataSource, UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UITextField *issueType;
@property (strong, nonatomic) IBOutlet UIButton *uploadImageButton;
@property (nonatomic, strong) UIPickerView *issueTypePickerView;
@property (strong, nonatomic) IBOutlet UILabel *NameLabel;




@property (nonatomic,strong)NSArray *issueTypes;
@end

@implementation ReportFormTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
  self.Name =  [userDefault valueForKey:@"n"];
    
    
    
    
    self.NameLabel.text = self.Name;
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1.0/255.0 green:181.0/255.0 blue:0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    self.issueTypes= [NSArray arrayWithObjects:@"Dirty", @"Broken", @"Missing",@"Needs Cleaning",@"Vandalised",@"Not-accessible",nil];
    
    _uploadImageButton.layer.borderWidth = 1.0f;
    _uploadImageButton.layer.borderColor = [UIColor blackColor].CGColor;
    
    
   self.issueTypePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-200, 320, 200)];
       self.issueTypePickerView.delegate = self;
       self.issueTypePickerView.dataSource = self;
       self.issueTypePickerView.showsSelectionIndicator = YES;
    [self.issueType setInputView:   self.issueTypePickerView];
    self.issueType.delegate = self;
    
    
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    
    self.issueType.text = [self.issueTypes objectAtIndex:row];
    [self.issueType resignFirstResponder];
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = 5;
    
    return self.issueTypes.count;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;
    title = [self.issueTypes objectAtIndex:row];
    
    return title;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}


-(void) dateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.issueType.inputView;
    [picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    self.issueType.text = [NSString stringWithFormat:@"%@",dateString];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)uploadImageClicked:(id)sender {
    
    
    [self showActionsheet];
    
    
    
    
    
    
    
}


- (void)showActionsheet {
    
    [self.issueType resignFirstResponder];
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Action Sheet" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
        
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera Roll" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // camera roll button tapped.
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // OK button tapped.
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma - Image delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
     self.imageView.image = info[UIImagePickerControllerEditedImage];
    
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (IBAction)submitTheReport:(id)sender {
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:YES forKey:@"sr"];
    [userDefaults synchronize];
    
    NSString *issueTye = self.issueType.text;
    [self dismissViewControllerAnimated:YES completion:nil];
    NSString *latString = [NSString stringWithFormat:@"%f",self.fac.lat];
    NSString *longst = [NSString stringWithFormat:@"%f",self.fac.longt];
    GovHackServices *govHack = [[GovHackServices alloc] init];
    NSDictionary *params = @{@"ref":@"",
                             @"type" : self.fac.type,
                             @"operator" : self.fac.operatorName,
                             @"name" : self.fac.name,
                             @"lon" : longst,
                             @"lat" : latString,
                             @"description" :issueTye,
                             @"photo" : @"http://www.google.com"
                             };
    
    
    
    
    
    
    
    
    
    [govHack postReport:params success:^(NSString *communityData) {
        NSLog(@"hurray");
    } failure:^(NSString *message) {
        NSLog(@"noooo");
    }];
    
    
    
    
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
  
}



/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
